<?php

$channels = $argv[1];
$apiKey = $argv[3];
$baseURL = "https://www.googleapis.com/youtube/v3/";

$list = explode(PHP_EOL, file_get_contents($channels));

$channeltitle = array();
$totalviews = array();
$videotitle = array();
$totalvalue = array();
$videoid = array();

if ($argc < 2 || in_array($argv[1], array('--help', '-help', '-h', 'help'))) { //additional switches for listing help
	echo "\nUsage: \n\n -----------------------------------------------------------\n|  php " . $argv[0] . " <input_file> <output_file> <API_key>  |\n -----------------------------------------------------------\n\n";
	echo "[1] <input_file> - specify the name of the *.txt file containing three YouTube channels IDs\n(store the file in the same directory from where you are running the phpYTscraper.php script)\n";
	echo "[2] <output_file> - specify the desired output file name (*.html)\n";
	echo "[3] <API_key> - specify the API key you want to utilize";
	echo "\n\noptional arguments: \n\n'show'                           shows the content of provided *.txt file\n'--help', '-help', '-h', 'help'  show this help message and exit\n";
}

elseif ($argv[1] == 'show') {
			echo "The content of " . $argv[2] . ":\n";
			echo file_get_contents( $argv[2] ); // echoing channel IDs from given file
}			


elseif ($argc > 3) {
foreach ($list as $youtubeChannelId)
{

	$URL = $baseURL . "search?part=snippet&channelId=" . $youtubeChannelId . "&order=viewCount&type=video&key={$apiKey}";
	$JSON = file_get_contents($URL);
	$json_data = json_decode($JSON, true);

	echo "\nMost viewed video title: " . $videoTitle = $json_data['items'][0]['snippet']['title'];
	$videotitle[] = $videoTitle; 			//assigning titles to the $videotitle array
	$videoId = $json_data['items'][0]['id']['videoId'];
	$videoid[] = $videoId; 					//assigning IDs to the $videoid array
	echo "\nVideo ID: " . $videoId;
	echo "\nLink: https://www.yotube.com/watch?v=" . $videoId;

	$URL2 = $baseURL . "videos?part=statistics&id=" . $videoId . "&key={$apiKey}";
	$JSON2 = file_get_contents($URL2);
	$json_data2 = json_decode($JSON2, true);

	echo "\nNumber of views: " . $views = $json_data2['items'][0]['statistics']['viewCount'];
	echo "\nLikes: " . $likes = $json_data2['items'][0]['statistics']['likeCount'];

	$URL3 = $baseURL . "channels?part=statistics&id=" . $youtubeChannelId . "&key={$apiKey}";
	$JSON3 = file_get_contents($URL3);
	$json_data3 = json_decode($JSON3, true);
	
	echo  "\n\nTotal number of views on the channel: " . $totalNumberOfViews = $json_data3['items'][0]['statistics']['viewCount'];
	$totalviews[] = $totalNumberOfViews; 	//assigning number of views to the $totalviews array
	echo "\nTotal number of videos on the channel: " . $totalNumberOfVideos = $json_data3['items'][0]['statistics']['videoCount'];
	echo "\nAverage number of views: " . $totalValue = $totalNumberOfViews / $totalNumberOfVideos;
	$totalvalue[] = $totalValue; 			//assigning values to the $totalvalue array

	$URL4 = $baseURL . "channels?part=snippet,statistics&id=" . $youtubeChannelId . "&key=AIzaSyAV21ghTd-sBlGYpmiY5kCqgMBd14wYBYY";
	$JSON4 = file_get_contents($URL4);
	$json_data4 = json_decode($JSON4, true);

	echo "\nChannel title: " . $channelTitle[] = $json_data4['items'][0]['snippet']['localized']['title'];
	$channeltitle[] = $channelTitle; 		//assigning channel titles to the $channeltitle array
	echo PHP_EOL;
	echo "----";
}

	echo "======\nVideo titles: ";	
	print_r($videotitle);	// print videotitle array 
	echo PHP_EOL;

$filename = $argv[2];
	$link_address1 = "https://youtube.com/watch?v=" . $videoid[0]; 		//link to 1st YT video
	$link_address2 = "https://youtube.com/watch?v=" . $videoid[1]; 		//link to 2nd YT video
	$link_address3 = "https://youtube.com/watch?v=" . $videoid[2]; 		//link to 3rd YT video
	
	// Save data to the $outputfile variable
	$outputfile = 
	"<!doctype html><html>
	<head><meta charset='utf-8'>
	<title>YouTube Stats</title> 
	</head>
	<body>
		<h3>Stats for channel: <b>$channelTitle[0]</b></h3>
		<p>Video name with the highest number of views: $videotitle[0]</p>
		<p>Link: <a href='$link_address1' target='_blank'> $link_address1 </a></p>
		<p>ID: $videoid[0]</p>
		<p>The average number of video views on the channel: $totalvalue[0]</p>
		<p>Total video views (summary):  $totalviews[0]</p>
		
		<h3>Stats for channel: $channelTitle[1]</h3>
		<p>Video name with the highest number of views: $videotitle[1]</p>
		<a href='$link_address2' target='_blank'> $link_address2 </a>
		<p>ID: $videoid[1]</p>
		<p>The average number of video views: $totalvalue[1]</p>
		<p>Total video views (summary):  $totalviews[1]</p>
		
		<h3>Stats for channel: $channelTitle[2]</h3>
		<p>Video name with the highest number of views: $videotitle[2]</p>
		<a href='$link_address3' target='_blank'> $link_address3 </a>
		<p>ID: $videoid[2]</p>
		<p>The average number of video views: $totalvalue[2]</p>
		<p>Total video views (summary):  $totalviews[2]</p>
	</body>
	</html>";
	
	// Writes the $outputfile back to the file
	file_put_contents($filename, $outputfile);
}

?>