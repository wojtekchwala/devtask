# **phpYTscraper**🎞️

The aplication consists of CLI .php script reading and processing given .txt file with channel IDs as an argument. After successful retrieval of statistics using the *YouTube API v3*, it creates a simple .html webpage to display them in a human-friendly format.  

**In CLI part**, the script allows you to obtain information about:  
1. Most viewed video title & video ID  
2. Total number of views and likes per channel  
3. Average number of views per channel  
4. Channel title  

**On the front end page**, you are presented with:  
1. Video name with the highest number of views  
2. Link to the video  
3. ID of the video  
4. The average number of video views  
3. Total video views (summary)  

*We strongly recommend that you open this README in another tab as you perform the initial test of the presented tool.

---

## Usage
Invoke from terminal:

`php phpYTscraper.php <input_file> <output_file> <API_key>`

* <input_file> - name of .txt file with channel IDs (line-by-line)  keep the file in the same directory from where you are running the script
* <output_file> - desired output .html file name 
* <API_key> - your API key


Example:


Prepare list with channel IDs (line-by-line)📃
*youtubelist.txt*
```
UCY2qt3dw2TQJxvBrDiYGHdQ
UCu_NPt9-Ssze_QsB7vS50Mg
UCP-Ng5SXUEt0VE-TXqRdL6g
```


And run: 🚀💻👨🏽‍💻  
`php phpYTscraper.php youtubelist.txt frontpage.html <API_KEY>`  

---  

**PROTIP:**
The additional built-in feature allows you for getting even more information about the target. If you observe the console output closely while running the script, some useful information may catch your eye instantly! 